import { Inter } from 'next/font/google'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  const share = () => {
    if(navigator.share) {
      navigator?.share?.({
        title: 'title',
        text: 'title',
        url: window.location.href,
      })
    } else {
      alert('crash 3');
    }
  }

  return (
    <main
      className={`flex min-h-screen flex-col items-center justify-between p-24 ${inter.className}`}
    >
      <button onClick={share}>Share button</button>
    </main>
  )
}
